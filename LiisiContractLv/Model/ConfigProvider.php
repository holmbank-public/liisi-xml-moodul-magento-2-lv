<?php
namespace Veebipoed\LiisiContractLv\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class ConfigProvider implements ConfigProviderInterface
{
	protected $_assetRepo;

	public function __construct(\Magento\Framework\View\Asset\Repository $assetRepo) {
		$this->_assetRepo = $assetRepo;
	}

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [
            'payment' => [
                'liisi' => [
                    'paymentAcceptanceMarkSrc' => $this->_assetRepo->getUrl("Veebipoed_LiisiContractLv::images/logo.png")
                ]
            ]
        ];
        return $config;
    }
}