<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Veebipoed\LiisiContractLv\Model;



/**
 * Pay In Store payment method model
 */
class LiisiContractLv extends \Magento\Payment\Model\Method\AbstractMethod
{
    public $mode = "test";
    public $country = "latvia";
    public $username = "";
    public $password = "";
    public $certificate = "";

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'liisicontractlv';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;


    public function getLiisiAPI()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('Veebipoed\LiisiContractLv\Helper\Data');

        $this->mode = ($helper->getGeneralConfig('test_mode') ? "test" : "live");
        $this->username = $helper->getGeneralConfig('username');
        $this->password = $helper->getGeneralConfig('password');
        $this->country = $this->country;
        $this->certificate = $helper->getGeneralConfig('certificate');
    }

}
