<?php
namespace Veebipoed\LiisiContractLv\Observer;

use Magento\Framework\Event\ObserverInterface;

class LiisiObserverLv implements ObserverInterface
{
	protected $_responseFactory;
    protected $_url;


	public function __construct(\Magento\Framework\App\ResponseFactory $responseFactory, \Magento\Framework\UrlInterface $url)
	{
		$this->_responseFactory = $responseFactory;
		$this->_url = $url;
	}

	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$order_ids = $observer->getData('order_ids');
		$order_id = (count($order_ids)>0 ? $order_ids[0] : 0);
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);
		if (!$order) return null;
		$payment = $order->getPayment();
	    $method = $payment->getMethodInstance();
	    $method = $method->getCode();
	    if ($method == "liisicontractlv") {
	    	$order->setStatus("pending");
			$order->save();
			$CustomRedirectionUrl = $this->_url->getUrl('liisicontractlv/index/index');
            $this->_responseFactory->create()->setRedirect($CustomRedirectionUrl."?order_id=".$order_id)->sendResponse();
	    }
	}
}
?>